# Cisco + Ansible

**Ce document est destiné à être un exemple généraliste. Adaptez les IPs pour correspondre aux vôtres.**

<!-- vim-markdown-toc GitLab -->

* [0. Setup](#0-setup)
    * [Ansible control-node](#ansible-control-node)
    * [Pré-requis](#pré-requis)
    * [Environnement Ansible](#environnement-ansible)
* [I. Configuration de `r1`](#i-configuration-de-r1)
    * [Configuration des interfaces](#configuration-des-interfaces)
    * [Configuration du NAT](#configuration-du-nat)
* [II. Configuration de `r2`](#ii-configuration-de-r2)
    * [Configuration des interfaces](#configuration-des-interfaces-1)
* [III. Aller plus loiiin](#iii-aller-plus-loiiin)
    * [Configuration générale](#configuration-générale)
    * [Backup via Ansible](#backup-via-ansible)
    * [Entracte](#entracte)
* [IV. Write less, do more](#iv-write-less-do-more)

<!-- vim-markdown-toc -->

# 0. Setup

## Ansible control-node

Pour le `control-node` Ansible (la machine qui déploie les configurations)
* créez une VM (CentOS7)
* installez-y Ansible
* puis importez cette VM dans GNS3.

## Pré-requis

* une machine avec Ansible installé
* routeurs Cisco avec une IP et un SSH d'activé

**Pour l'accès SSH sur les routeurs** :
```cisco
# conf t

(config)# username <USER> password <PASSWORD>
(config)# username <USER> privilege 15

(config)# ip domain-name cesi

(config)# line vty 0 4
(config-line)# privilege level 15
(config-line)# password <PASSWORD>
(config-line)# login local
(config-line)# transport input ssh
(config-line)# exit


(config)# crypto key generate rsa
How many bits in the modulus [512]: 2048
[...]

(config)# exit
```

## Environnement Ansible

Sur le `control-node`, **créez un répertoire destiné à héberger tous vos playbooks Ansible.**

> Vous me mettez pas ça dans `/etc/ansible`, pitié :)

Dans ce répertoire, créez :

* un fichier `hosts.ini`
```ini
[cisco]
10.10.255.254
10.10.2.254
[r1]
10.10.255.254
[r2]
10.10.2.254
```

* un fichier `ansible.cfg`
```ini
[defaults]
inventory=./hosts.ini
host_key_checking = False 
retry_files_enabled = False

[ssh_connection]
ssh_args = -C -o ControlMaster=auto -o ControlPersist=30m
retries=3
pipelining=False
```

> La partie `ssh_connection` du `ansible.cfg` est utilisée dans un contexte Ansible + Cisco **dans GNS3**. En effet les IOS utilisés sont souvent un peu vieillissants et implémentent des versions de SSH plutôt obsolète.

* un dossier `group_vars/`

* un fichier `groups_vars/yml`
```yaml
---
ansible_network_os: "ios"
ansible_user: "<USER>"
ansible_password: "<PASSWORD>"
```

* un dossier `playbooks/`

---

**IMPORTANT**

Suivant le client SSH que vous avez, il est possible qu'il soit trop récent pour faire confiance à la cryptographie faible de certains IOS Cisco. Pour remédier à ça, il faudra configurer votre fichier `~/.ssh/config` pour explicitement accepter des méthodes de chiffrement considérées comme "faibles".

Par exemple :

```
Host 10.10.255.254
  KexAlgorithms +diffie-hellman-group1-sha1
  Ciphers +aes256-cbc,aes192-cbc,aes128-cbc,3des-cbc
```

# I. Configuration de `r1` 

Au menu :
* configuration des interfaces
* configuration du NAT

## Configuration des interfaces

> La suite admet que l'interface FastEthernet1/0 de `r1` pointe vers le réseau `guest1`.

Créez le fichier `playbooks/r1.yml`

```yaml
---
- hosts: `r1` 
  gather_facts: no
  connection: local

  tasks:
  - name: Configure fastEthernet1/0
    ios_config:
      parents: interface FastEthernet1/0
      lines:
        - description <DESCRIPTION>
        - ip address 10.10.1.254 255.255.255.0
        - duplex auto
        - speed auto
    register: config

  - name: Bring fastEthernet1/0 up
    ios_config:
      lines:
        - no shutdown
      parents: interface FastEthernet1/0
    when: config.changed
```

Prenez le temps de bien comprendre ce *playbook*, vous allez le modifier et l'étendre au fur et à mesure.

Exécutez le *playbook* :
```bash
$ ansible-playbook playbooks/r1.yml
```

Modifiez le *playbook* pour :
* ajouter la configuration de l'interface qui pointe vers le réseau r1-r2
* ajouter la configuration de l'interface qui pointe vers le NAT

## Configuration du NAT

> La suite admet que l'interface FastEthernet1/0 de `r1` pointe vers le réseau `guest1` et FastEthernet0/0 vers le NAT.

Pour que les machines du parc puissent accéder au WAN, il va falloir configurer `r1` pour faire du source NAT.

Les commandes sur Cisco pour mettre en place ce genre de NAT de façon simple :
```cisco
! On s'assure de pouvoir ping le WAN depuis le routeur
# ping 1.1.1.1

# conf t

(config)# interface fastEthernet0/0
(config-if)# ip nat outside
(config-if)# exit

(config)# interface fastEthernet1/0
(config-if)# ip nat inside
(config-if)# exit

(config)# ip nat inside source list 1 interface FastEthernet0/0 overload
(config)# access-list 1 permit any
```

* modifiez notre ami `playbooks/r1.yml` pour intégrer la configuration NAT
  * l'interface qui pointe vers le WAN est en `outside`
  * toutes les autres sont en `inside`


# II. Configuration de `r2` 

## Configuration des interfaces

Créez le *playbook* `playbooks/r2.yml`.

*You know what you got to do.*
* configuration interfaces manquantes
* configuration route par défaut

# III. Aller plus loiiin

## Configuration générale

**Créez un *playbook* de configuration générale** : `playbooks/cisco.yml`
* définissez un serveur DNS à utiliser (`1.1.1.1` par exemple)
* autres si vous le souhaitez

## Backup via Ansible

Créez le dossier `backups/`.

Un beau *playbook* `playbooks/backup-cisco.yml` :

```yml
---
- hosts: cisco
  gather_facts: no
  connection: local

  tasks:
  - name: Store running-config
    ios_command:
      commands:
        - show running-config all
    register: config

  - name: Save running-config on control node
    copy: 
      content: "{{ config.stdout[0] }}"
      dest: "backups/show_run_{{ inventory_hostname }}.txt"
```

* lisez et appréhendez le *playbook*
* exécutez le *playbook*
* ajoutez une sauvgarde de l'état des interfaces

---

Pour conserver l'idempotence des fichiers Ansible, il est nécessaire d'utiliser strictement les commandes en entier. C'est à dire :
* `shutdown` et pas `shut`
* `show running-config` et pas `show run`
* etc.

En effet, pour tester si la config existe déjà, Ansible va regarder l'état de la `running-config` et `startup-config`. Or, dans ces dernières, les commandes ne sont pas tronquées et écrites en entier. On doit faire de même pour que la comparaison puisse être effectuée correctement.

## Entracte

A ce stade on peut faire...
```bash
# Déploiement de `r1` et `r2` 
$ ansible-playbook -i hosts.ini playbooks/r1.yml playbooks/r2.yml

# Déploiement de la conf générale
$ ansible-playbook -i hosts.ini playbooks/cisco.yml

# Backup de la conf
$ ansible-playbook -i hosts.ini playbooks/backup-cisco.yml

# Conf + Backup de tout
$ ansible-playbook -i hosts.ini playbooks/cisco.yml playbooks/r1.yml playbooks/r2.yml playbooks/backup-cisco.yml
```

C'est pas beau ? :)  
Et tout ça est idempotent !

# IV. Write less, do more

* **boucle de config**
  * créez un dossier `host_vars/`
  * stockez-y sous forme de `.yml` la configuration redondante
    * la configuration de vos cartes réseau par exemple
  * utilisez une boucle dans les *playbooks* `playbooks/r1.yml` et `playbooks/r2.yml` pour configurer toutes les interfaces
* ***handler*** *Ansible*
  * utilisez un *handler* Ansible plutôt qu'une simple *task* pour `no shut` les interfaces quand elles sont modifiées
* **stockez les mots de passe dans un Vault Ansible**
* **créez et utilisez un répertoire `inventory/`**
  * c'est lui qui contient l'inventaire
  * mais aussi `host_vars/` et `group_vars/`
